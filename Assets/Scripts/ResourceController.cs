using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceController : MonoBehaviour
{
     [SerializeField] private Image resourceImage;
     
     [SerializeField] private Text resourceDescription;
     [SerializeField] private Text resourceUpgradeCost;
     [SerializeField] private Text resourceUnlockCost;

     private GameManager.ResourceConfig m_config;
     private int m_index;

     private int Level
     {
          set
          {
               UserDataManager.Progress.resourcesLevels[m_index] = value;
               UserDataManager.Save(true);
          }

          get
          {
              
               if (!UserDataManager.HasResources(m_index))
               {
                    return 1;
               }

               var resources = UserDataManager.Progress.resourcesLevels;
               return resources[m_index];
          
          }
     }
     
     private Button m_button;

     public Image ResourceImage => resourceImage;
     public bool IsUnlocked { get; private set; }

     private void Awake()
     {
          m_button = GetComponent<Button>();
     }

     private void Start()
     {
          m_button.onClick.AddListener(() =>
          {
               if (IsUnlocked)
               {
                    UpgradeLevel();
               }
               else
               {
                    UnlockResource();
               }
          });
     }

   

     public void SetConfig(int index, GameManager.ResourceConfig config)
     {
          m_index = index;
          m_config = config;
          resourceDescription.text = $"{m_config.name} Lv.{Level}\n+{GetOutput():0}";
          resourceUnlockCost.text = $"Unlock Cost\n{GetUnlockCost():0}";
          resourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost():0}";

          SetUnlocked(m_config.unlockCost == 0 || UserDataManager.HasResources(m_index));
     }

     private void UnlockResource()
     {
          var unlockCost = GetUnlockCost();
          if( UserDataManager.Progress.gold < unlockCost) return;
          
          SetUnlocked(true);
          GameManager.Instance.ShowNextResource();

          AchievementController.Instance.UnlockAchievementType(AchievementController.AchievementType.UnlockResource,
               m_config.name);
          
          AnalyticsManager.LogUnlockEvent(m_index);
     }

     private void UpgradeLevel()
     {
          var upgradeCost = GetUpgradeCost();
          if( UserDataManager.Progress.gold < upgradeCost) return;
          GameManager.Instance.AddGold(-upgradeCost);
          Level++;
          
          resourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost()}";
          resourceDescription.text = $"{m_config.name} Lv. {Level}\n+{GetOutput():0}";
          
          AnalyticsManager.LogUpgradeEvent(m_index, Level);
     }
     private void SetUnlocked(bool unlocked)
     {
          IsUnlocked = unlocked;
          if (unlocked)
          {
               if (!UserDataManager.HasResources(m_index))
               {
                    UserDataManager.Progress.resourcesLevels.Add(Level);
                    UserDataManager.Save(true);
               }
          }
          resourceImage.color = IsUnlocked ? Color.white : Color.grey;
          resourceUnlockCost.gameObject.SetActive(!unlocked);
          resourceUpgradeCost.gameObject.SetActive(unlocked);
          
     }

     public double GetOutput()
     {
          return m_config.output * Level;
     }

     public double GetUpgradeCost()
     {
          return m_config.upgradeCost * Level;
     }

     public double GetUnlockCost()
     {
          return m_config.unlockCost;
     }
}
