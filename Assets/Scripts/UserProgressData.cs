﻿
using System;
using System.Collections.Generic;

[Serializable]
public class UserProgressData
{
    public double gold = 0;
    public List<int> resourcesLevels = new List<int>();
}