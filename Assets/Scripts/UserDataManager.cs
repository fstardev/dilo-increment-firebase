﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Firebase.Storage;
using UnityEngine;

public static class UserDataManager
{
    private const string ProgressKey = "Progress000111";

    public static UserProgressData Progress;

    public static IEnumerator LoadFromCloud(Action onComplete)
    {
        var targetStorage = GetTargetCloudStorage();

        var isCompleted = false;
        var isSuccessFull = false;
        const long maxAllowedSize = 1024 * 1024;
        targetStorage.GetBytesAsync(maxAllowedSize).ContinueWith((task =>
        {
            if (!task.IsFaulted)
            {
                var json = Encoding.Default.GetString(task.Result);
                Progress = JsonUtility.FromJson<UserProgressData>(json);
                isSuccessFull = true;
            }

            isCompleted = true;
        }));

        while (!isCompleted)
        {
            yield return null;
        }

        if (isSuccessFull)
        {
            Save();
        }
        else
        {
            LoadFromLocal();
        }
        
        onComplete?.Invoke();
    }
    
    public static void LoadFromLocal()
    {
        if (!PlayerPrefs.HasKey(ProgressKey))
        {
            Debug.Log("new progressd");
            Progress = new UserProgressData();
            Save(true);
        }
        else
        {
            var json = PlayerPrefs.GetString(ProgressKey);
            Progress = JsonUtility.FromJson<UserProgressData>(json);
        }
    }

    private static StorageReference GetTargetCloudStorage()
    {
        var deviceID = SystemInfo.deviceUniqueIdentifier;
        var storage = FirebaseStorage.DefaultInstance;

        return storage.GetReferenceFromUrl($"{storage.RootReference}/{deviceID}");
    }

    public static void Save(bool uploadToCloud = false)
    {
        var json = JsonUtility.ToJson(Progress);
        PlayerPrefs.SetString(ProgressKey, json);

        if (uploadToCloud)
        {
            AnalyticsManager.SetUserProperties("gold", $"{Progress.gold:0}");
            
            var data = Encoding.Default.GetBytes(json);
            var targetStorage = GetTargetCloudStorage();
            targetStorage.PutBytesAsync(data);
        }
    }

    public static bool HasResources(int index)
    {
        var count = Progress.resourcesLevels.Count;
        return  index < count;
    }
}